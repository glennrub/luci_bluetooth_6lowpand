Overview
========
OpenWRT LuCI interface for administrating the bluetooth_6lowpand.

Requirements
============
* HCI compatible bluetooth dongle (bluetooth 4.0)
* bluetooth_6lowpand installed (http://github.com/NordicSemiconductor/Linux-ble-6lowpan-joiner)

Installation
============
* Copy the files directly to /usr/lib/lua/luci. 
* Remove temporary luci files from /tmp: 
```
#!bash
$ cd /tmp
$ rm -r luci-*
```
* Start the LuCI interface in the browser and navigate to the BLE section. 

Screenshot
==========
![luci_bluetooth_6lowpan_daemon_interface.png](https://bitbucket.org/repo/q4bXE4/images/1254538715-luci_bluetooth_6lowpan_daemon_interface.png)